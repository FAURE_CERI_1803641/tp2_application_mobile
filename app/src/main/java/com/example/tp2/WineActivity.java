package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WineActivity extends AppCompatActivity {

    WineDbHelper wineTable = new WineDbHelper(WineActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        Intent intent = getIntent();
        final Wine wine = (Wine) intent.getSerializableExtra("wine");


        final EditText wineName = (EditText) findViewById(R.id.wineName);

        final EditText editWineRegion = (EditText) findViewById(R.id.editWineRegion);

        final EditText editLoc = (EditText) findViewById(R.id.editLoc);

        final EditText editClimate = (EditText) findViewById(R.id.editClimate);

        final EditText editPlantedArea = (EditText) findViewById(R.id.editPlantedArea);



        wineName.setText(wine.getTitle());

        editWineRegion.setText(wine.getRegion());

        editLoc.setText(wine.getLocalization());

        editClimate.setText(wine.getClimate());

        editPlantedArea.setText(wine.getPlantedArea());

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                wine.setTitle(wineName.getText().toString());
                wine.setRegion(editWineRegion.getText().toString());
                wine.setLocalization(editLoc.getText().toString());
                wine.setClimate(editClimate.getText().toString());
                wine.setPlantedArea(editPlantedArea.getText().toString());

                wineTable.updateWine(wine);

                finish();

            }
        });
    }
}
