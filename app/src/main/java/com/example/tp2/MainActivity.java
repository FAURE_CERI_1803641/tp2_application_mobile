package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Creation et formation de la base de donnée:
    WineDbHelper wineTable = new WineDbHelper(MainActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.addWine);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();



        }

        });


        Cursor cursor = wineTable.fetchAllWines();
        final ListView listview = (ListView) findViewById(R.id.winesList);
        SimpleCursorAdapter adapter =
                new SimpleCursorAdapter(this,
                        R.layout.wine_display,
                        cursor, new String[] {
                        wineTable.COLUMN_NAME,
                        wineTable.COLUMN_WINE_REGION},
        new int[] { R.id.wineName, R.id.wineRegion }, 0);
        listview.setAdapter(adapter);



        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(parent.getContext(), WineActivity.class);
                Cursor wineSelected = (Cursor) parent.getItemAtPosition(position);
                Wine wine = wineTable.cursorToWine(wineSelected);
                intent.putExtra("wine", wine);

                Toast.makeText(MainActivity.this, "Vous avez sélectionné: " +  wine.getTitle(), Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });

        final Button addWine = (Button) findViewById(R.id.addWine);
        addWine.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(this, WineActivity.class);
                Wine wine = new Wine();
                intent.putExtra("newWine", wine);
                Toast.makeText(MainActivity.this, "Création d'un nouveau vin ", Toast.LENGTH_LONG).show();
                startActivity(intent);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
